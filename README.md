# Oh Dash

Rough proof of concept heavily inspired by [HTMX](https://htmx.org/)

Adds AJAX and other capabilities to your HTML with attributes

```html
<form id="form-1" o-post="/items" o-replace-inner="#output">
  <input name="first" />
  <input name="second" />
  <button o-click="#form-1">Submit</button>
</form>
<div id="output"></div>
```

What separates it from Htmx

- You can map parts of the response to parts of the DOM `o-replace-inner="#response-part-a -> .list-items; #user-info -> #user-info"`
- The server can add instructions in the response header like `o-append: #validation-errors -> #alerts`
- You don't have to only deal with HTML from the server `o-find="#confirmation-modal" o-replace-inner="#modal-slot"`

How it works:

- On page load Oh-dash adds event listeners to all trigger attributes like `o-click`
- When the user clicks on the button, Oh-dash will find the element the trigger points to, in this case `#form-1`
- Oh-dash will collect all `o-` attributes on the target and preform each instruction
- In this case it will send a post request to `/items` and include all the inputs within the form.
- When the response returns it will replace the inner html of the `#output` element

Another example

```html
<button o-click o-get="/items" o-append="#item-list">Append item</button>
<div id="item-list"></div>
```

## Commands so far

Triggers

- o-click:
- o-submit

Actions

- o-get
- o-post
- o-find

Instructions

- o-replace
- o-replace-inner
- o-remove
- o-remove-inner
- o-append

Helpers

- o-hide
- o-listener

## Dev setup

```sh
# Install node packages
npm i

# Start dev server at http://localhost:3000/
npm run dev

# In another terminal start backend server at http://localhost:4000
npm run server
```
