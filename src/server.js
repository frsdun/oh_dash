const express = require('express')
var cors = require('cors')
const app = express()
const port = 4000

app.use(cors())
app.use(express.json())

app.get('/items', (req, res) => {
  res.send(`<div>ITEM</div>`)
})
app.get('/info', (req, res) => {
  res.send(`<div id="part-a">PART A</div><div id="part-b">PART B</div>`)
})
app.post('/items', (req, res) => {
  const one = req.body?.inputOne || "missing";
  const two = req.body?.inputTwo || "missing";
  res.send(`<div>ITEM inputOne=${one}, inputTwo=${two}</div>`)
})
app.get('/books', (req, res) => {
  res.set({
    "Access-Control-Expose-Headers":"o-replace-inner",
    'o-replace-inner': '#test-8',
  })
  res.send(`<div>BOOK</div>`)
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})