interface iAction {
  name: string; // Name of attribute
  values: string[]; // Value of attribute
  func: (...args: any[]) => any;
  event: Event;
  instructor: HTMLElement;
  instructions: iInstruction[];
}

interface iInstruction {
  name: string; // Name of attribute
  values: string[]; // Value of attribute
  func: (...args: any[]) => any;
  instructor: HTMLElement;
  // Possible params for the func
  url?: string;
  html?: string;
}

function addListeners(target?: HTMLElement) {
  console.log(`o-addListeners to ${target?.tagName || "document"}`);
  // Attach all triggers
  attachTrigger("click", "o-click", target)
  attachTrigger("submit", "o-submit", target)
}

function attachTrigger(eventName = "click", attributeName = "o-click", target?: HTMLElement) {
  // attached event listeners to all elements with the attributeName
  (target || document).querySelectorAll(`[${attributeName}]`).forEach(element => {
    if (element.getAttribute("o-listener") === attributeName) return
    element.addEventListener(eventName, (e) => {
      e.preventDefault()
      oTrigger(e, attributeName, element.getAttribute(attributeName))
    })
    element.setAttribute("o-listener", attributeName)
  })
}

function oTrigger(event: Event, attributeName: string, attributeValue: string | null) {
  // Finds the instructor element, extracts the instructions and the starts each instruction.
  // oTrigger is fired by event listeners which are attached in the addListeners() function.

  console.log(`${attributeName}=${attributeValue}`);

  // Find instructor
  // Can only have one instructor for now
  let instructor = null as HTMLElement | null;
  if ((!attributeValue || attributeValue === "" || attributeValue === "self") && event.target instanceof HTMLElement) instructor = event.target;
  // Too much magic
  // else if ((!attributeValue || attributeValue === "") && event.target instanceof HTMLElement) instructor = event.target.closest("form")
  else instructor = document.querySelector(attributeValue || "");
  if (!instructor) {
    console.log("No container found for selector", attributeValue)
    return;
  }

  // Process oh-dash attributes, find the action to do and create instruction list
  const allAttributeNames = instructor.getAttributeNames().filter(name => name.startsWith("o-"))
  console.log(`o-instructor=${instructor.tagName} instruction=[${allAttributeNames.join(", ")}]`)

  // Build instructions list
  const instructions: iInstruction[] = [];
  allAttributeNames.forEach(name => {
    if (!instructor) return;
    const values = splitAttributeValue(instructor.getAttribute(name));
    switch (name) {
      case "o-replace": instructions.push({ name, values, func: oReplace, instructor }); break;
      case "o-replace-inner": instructions.push({ name, values, func: oReplaceInner, instructor }); break;
      case "o-remove": instructions.push({ name, values, func: oRemove, instructor }); break;
      case "o-remove-inner": instructions.push({ name, values, func: oRemoveInner, instructor }); break;
      case "o-append": instructions.push({ name, values, func: oAppend, instructor }); break;
    }
  });

  // Find action
  let action = null as iAction | null;
  allAttributeNames.forEach(name => {
    if (!instructor) return;
    const values = splitAttributeValue(instructor.getAttribute(name));
    switch (name) {
      case "o-get": action = { name, values, func: oGet, event, instructor, instructions }; break;
      case "o-post": action = { name, values, func: oPost, event, instructor, instructions }; break;
      case "o-find": action = { name, values, func: oFind, event, instructor, instructions }; break;
    }
  });

  // If there is an action do it, otherwise do all the instructions
  if (action) {
    action.func(action)
  } else {
    console.log("No action found on the instructor, continuing with instructions")
    instructions.forEach((instruction) => {
      instruction.func(instruction)
    })
  }
}


// ------------------------------------
// Actions
// ------------------------------------

function oGet(action: iAction) {
  logActions(action)
  // Sends a GET request
  // Only accepts a single url string as its attribute value
  const [url] = action.values
  fetch(url).then((res) => {
    const serverInstructions: iInstruction[] = []
    const serverReplaceInner = res.headers.get('o-replace-inner')
    if (serverReplaceInner) {
      serverInstructions.push({ name: "o-replace-inner", values: [serverReplaceInner], func: oReplaceInner, instructor: action.instructor })
    }

    res.text().then((html) => {
      if (serverInstructions.length > 0) {
        console.log("following server instructions")
        serverInstructions.forEach((instruction) => {
          instruction.html = html;
          instruction.func(instruction)
        })
      } else {
        action.instructions.forEach((instruction) => {
          instruction.html = html;
          instruction.func(instruction)
        })
      }
    });
  })
}

function oPost(action: iAction) {
  logActions(action)
  // Sends a POST request
  // Only accepts a single url string as its attribute value
  // Second value could be a JSON string? to merge in with collected inputs?
  const [url] = action.values
  const inputs = collectInputs(action.instructor)
  fetch(url, {
    method: "POST",
    body: JSON.stringify(inputs),
    headers: {
      'Content-Type': 'application/json'
    },
  }).then((res) => {
    res.text().then((html) => {
      action.instructions.forEach((instruction) => {
        instruction.html = html;
        instruction.func(instruction)
      })
    });
  })
}

function oFind(action: iAction) {
  logActions(action)
  // Finds an HTML element in the document
  // Only accepts a single css selector string as its attribute value
  const [selector] = action.values
  document.querySelectorAll(selector).forEach((x) => {
    const el = x.cloneNode(true) as HTMLElement
    el.classList.remove("o-hide");
    // Clone does not clone event listeners, so we must remove the listening flags (o-listener)
    el.querySelectorAll("[o-listener]").forEach(x => x.removeAttribute("o-listener"))
    el.removeAttribute("o-listener")

    action.instructions.forEach((instruction) => {
      instruction.html = el.outerHTML;
      instruction.func(instruction)
    })
  })
}

// ------------------------------------
// Instructions
// ------------------------------------

function oReplace(instruction: iInstruction) {
  logInstruction(instruction)
  instruction.values.forEach(selector => {
    // TODO test for " -> "
    document.querySelectorAll(selector).forEach(x => {
      if (!instruction.html) return;
      x.outerHTML = instruction.html
    })
  })
  addListeners()
}

function oReplaceInner(instruction: iInstruction) {
  logInstruction(instruction)
  instruction.values.forEach(selector => {
    // TODO test for " -> "
    if (selector.includes(" -> ")) {
      const [responseSelector, pageSelector] = selector.split(" -> ");
      if (!responseSelector || !pageSelector) {
        console.log(`Invalid selector ${selector}`)
        return;
      };
      if (!instruction.html) return;
      // const htmlNode = createElementFromHTML(instruction.html);
      const tempNode = document.createElement("div");
      tempNode.style.display = "none"
      tempNode.style.visibility = "0"
      tempNode.innerHTML = instruction.html;
      document.body.appendChild(tempNode)

      const responsePart = document.querySelector(responseSelector);
      if (!responsePart) {
        console.log(`No part found: selector=${responseSelector} in ${instruction.html}`)
        return;
      };

      document.querySelectorAll(pageSelector).forEach(pagePart => {
        if (!responsePart) return;
        pagePart.innerHTML = responsePart.outerHTML
      })

      tempNode.remove()

    } else {
      document.querySelectorAll(selector).forEach(x => {
        if (!instruction.html) return;
        x.innerHTML = instruction.html
      })
    }
  })
  addListeners()
}

function oRemove(instruction: iInstruction) {
  logInstruction(instruction)
  // TODO test for " -> "
  instruction.values.forEach(selector => {
    if (selector === "self") instruction.instructor.outerHTML = ""
    else document.querySelectorAll(selector).forEach(x => x.outerHTML = "")
  })
}

function oRemoveInner(instruction: iInstruction) {
  logInstruction(instruction)
  // TODO test for " -> "
  instruction.values.forEach(selector => {
    if (selector === "self") instruction.instructor.innerHTML = ""
    else document.querySelectorAll(selector).forEach(x => x.innerHTML = "")
  })
}

function oAppend(instruction: iInstruction) {
  logInstruction(instruction)
  instruction.values.forEach(selector => {
    // TODO test for " -> "
    document.querySelectorAll(selector).forEach(x => {
      if (!instruction.html) return;
      const el = createElementFromHTML(instruction.html);
      x.appendChild(el)
    })
  })
  addListeners()
}


addListeners()

// Helpers

function splitAttributeValue(value: string | null | undefined) {
  if (!value) return []
  return value.split(";").map(x => x.trim())
}

function createElementFromHTML(htmlString: string) {
  var div = document.createElement('div');
  div.innerHTML = htmlString.trim();
  return div.firstChild as HTMLElement;
  // return new DOMParser().parseFromString(htmlString, "text/html");
}

function collectInputs(container: HTMLElement) {
  // Collect inputs
  // TODO: find selects, checkboxes and other odd types
  const inputs: Record<string, any> = Array.from(container.querySelectorAll("input")).reduce(
    (obj: any, input) => {
      obj[input.name] = input.value
      return obj
    },
    {}
  )
  return inputs
}

function logActions(action: iAction) {
  console.log(`${action.name}=[${action.values.join(", ")}]`)
}
function logInstruction(instruction: iInstruction) {
  console.log(`${instruction.name}=[${instruction.values.join(", ")}] ${instruction.html?.substr(0, 20)}...`)
}
